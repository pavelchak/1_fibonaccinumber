package com.pavelchak;

import javafx.collections.transformation.FilteredList;

import java.util.ArrayList;
import java.util.List;

public class FibonacciNumber {
    private List<Integer> fibonacciNumber;
    private int F1; //odd fibonacci number
    private int F2; //even fibonacci number

    public FibonacciNumber(int size) {
        generateFibonacciNumber(size);
    }

    //  F(n)=F(n-1)+F(n-2)
    // 0,1,1,2,3,5,8,13,21,34,55,89,144,...
    private void generateFibonacciNumber(int size) {
        fibonacciNumber=new ArrayList<>();
        fibonacciNumber.add(0);
        fibonacciNumber.add(1);
        F1 = 1;
        F2 = 0;
        for (int i = 2; i < size; i++) {
            int newNumber = fibonacciNumber.get(i - 1) + fibonacciNumber.get(i - 2);
            fibonacciNumber.add(newNumber);
            checkMaxOddEven(newNumber);
        }
    }

    private int addFibonacciNumber() {
        int lastIndex = fibonacciNumber.size();
        int newNumber = fibonacciNumber.get(lastIndex) + fibonacciNumber.get(lastIndex - 1);
        fibonacciNumber.add(newNumber);
        checkMaxOddEven(newNumber);
        return newNumber;
    }

    private void checkMaxOddEven(int newNumber) {
        if (newNumber % 2 == 1) {
            F1 = newNumber;
        } else {
            F2 = newNumber;
        }
    }

    public List<Integer> getFibonacciNumber() {
        return fibonacciNumber;
    }

    public int getF1() {
        return F1;
    }

    public int getF2() {
        return F2;
    }

    public double getPercentageOdd(){
        int amountOdd=0;
        for(Integer number : fibonacciNumber){
            if(number%2==1){
                amountOdd++;
            }
        }
        return (double)amountOdd/fibonacciNumber.size()*100;
    }
}
