package com.pavelchak;

import java.util.ArrayList;
import java.util.List;

public class OrdinaryNumber {
    private int bottomLimit;
    private int topLimit;
    private List<Integer> odd;
    private List<Integer> even;

    public OrdinaryNumber(int bottomLimit, int topLimit) {
        this.bottomLimit = bottomLimit;
        this.topLimit = topLimit;
        this.odd = new ArrayList<>();
        this.even = new ArrayList<>();
        genereateOddEvenNumber();
    }

    private void genereateOddEvenNumber() {
        odd.clear();
        even.clear();
        for (int number = bottomLimit; number <= topLimit; number++) {
            if (number % 2 == 1) {
                odd.add(number);
            } else {
                even.add(0,number);
            }
        }
    }

    public int sumOddEvenNumbers() {
        int sum = 0;
        for (int element : odd) {
            sum += element;
        }
        for (int element : even) {
            sum += element;
        }
        return sum;
    }

    public int getBottomLimit() {
        return bottomLimit;
    }

    public void setBottomLimit(int bottomLimit) {
        this.bottomLimit = bottomLimit;
        genereateOddEvenNumber();
    }

    public int getTopLimit() {
        return topLimit;
    }

    public void setTopLimit(int topLimit) {
        this.topLimit = topLimit;
        genereateOddEvenNumber();
    }

    public List<Integer> getOdd() {
        return odd;
    }

    public List<Integer> getEven() {
        return even;
    }

}
