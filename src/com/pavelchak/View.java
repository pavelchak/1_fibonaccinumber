package com.pavelchak;

import com.pavelchak.Exceptions.IncorrectDataException;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
    private OrdinaryNumber ordinaryNumber;
    private FibonacciNumber fibonacciNumber;
    private Map<String, String> menu;
    private static Scanner input = new Scanner(System.in);

    public View() {
        menu = new LinkedHashMap<>();
        menu.put("C", "  C - create ordinary number list (ONL)");
        menu.put("O", "  O - print odd numbers for ONL");
        menu.put("E", "  E - print even numbers for ONL");
        menu.put("S", "  S - print sum of odd & even numbers for ONL");
        menu.put("F", "  F - create Fibonacci number list (FNL)");
        menu.put("P", "  P - print Fibonacci number list");
        menu.put("U", "  U - print F1 & F2 for Fibonacci numbers");
        menu.put("D", "  D - print percentage of odd & even FNL");
        menu.put("Q", "  Q - exit");
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values())
            System.out.println(str);
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            switch (keyMenu) {
                case "C":
                    createONL();
                    break;
                case "O":
                    printOddONL();
                    break;
                case "E":
                    printEvenONL();
                    break;
                case "S":
                    printSumOddEvenONL();
                    break;
                case "F":
                    createFNL();
                    break;
                case "P":
                    printFNL();
                    break;
                case "U":
                    printF1F2FNL();
                    break;
                case "D":
                    printPercentageOddEvenFNL();
                    break;
            }
        } while (!keyMenu.equals("Q"));
    }

    private void createONL() {
        boolean condition;
        String str;
        int bottomLimit;
        int topLimit;
        do {
            try {
                condition = false;
                System.out.println("Input bottom limit:");
                str = input.nextLine();
                bottomLimit = Integer.parseInt(str);

                System.out.println("Input top limit:");
                str = input.nextLine();
                topLimit = Integer.parseInt(str);
                if (topLimit <= bottomLimit) {
                    throw new IncorrectDataException();
                }
                ordinaryNumber = new OrdinaryNumber(bottomLimit, topLimit);
            } catch (IncorrectDataException e) {
                System.out.println("Incorrect limits!");
                condition = true;
            } catch (NumberFormatException e) {
                System.out.println("This is not number format");
                condition = true;
            }

        } while (condition);
    }

    private void printOddONL() {
        if (ordinaryNumber != null) {
            System.out.println("odd numbers for ONL:");
            for (Integer odd : ordinaryNumber.getOdd()) {
                System.out.printf("%d ", odd);
            }
        } else {
            System.out.println("ONL object is not created yet");
        }
    }

    private void printEvenONL() {
        if (ordinaryNumber != null) {
            System.out.println("even numbers for ONL:");
            for (Integer even : ordinaryNumber.getEven()) {
                System.out.printf("%d ", even);
            }
        } else {
            System.out.println("ONL object is not created yet");
        }
    }

    private void printSumOddEvenONL() {
        if (ordinaryNumber != null) {
            System.out.printf("sum of odd & even numbers: %d ", ordinaryNumber.sumOddEvenNumbers());
        } else {
            System.out.println("ONL object is not created yet");
        }
    }

    private void createFNL() {
        boolean condition;
        String str;
        int sizeFibonacci;
        do {
            try {
                condition = false;
                System.out.println("Input size FNL:");
                str = input.nextLine();
                sizeFibonacci = Integer.parseInt(str);
                if (sizeFibonacci <= 0) {
                    throw new IncorrectDataException();
                }
                fibonacciNumber = new FibonacciNumber(sizeFibonacci);
            } catch (IncorrectDataException e) {
                System.out.println("Incorrect size!");
                condition = true;
            } catch (NumberFormatException e) {
                System.out.println("This is not number format");
                condition = true;
            }
        } while (condition);
        printFNL();
    }

    private void printFNL() {
        if (fibonacciNumber != null) {
            System.out.println("Fibonacci number list:");
            for (Integer number : fibonacciNumber.getFibonacciNumber()) {
                System.out.printf("%d ", number);
            }
        } else {
            System.out.println("FNL object is not created yet");
        }
    }

    private void printF1F2FNL() {
        if (fibonacciNumber != null) {
            System.out.printf("F1=%d, F2=%d", fibonacciNumber.getF1(), fibonacciNumber.getF2());
        } else {
            System.out.println("FNL object is not created yet");
        }
    }

    private void printPercentageOddEvenFNL() {
        if (fibonacciNumber != null) {
            System.out.printf("odd=%.2f%%, even=%.2f%%", fibonacciNumber.getPercentageOdd(), 100 - fibonacciNumber.getPercentageOdd());
        } else {
            System.out.println("FNL object is not created yet");
        }
    }
}
